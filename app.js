const express = require('express')
const { exec } = require('child_process');
var bodyParser = require('body-parser')

const app = express()

app.all('/', function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      next();
});

app.use(bodyParser.urlencoded({extended: true}));
app.post('/', function (req, res) {
    const cmd = exec(req.body.command);
    cmd.stdout.on('data', (data) => {
        res.contentType("text/plain")
        res.send(data);
    });
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
